/**
 * @file
 * @author Lennart Yseboodt
 * @author Michael De Nil
 * @date 2006
 * @brief These are general filesystem functions, supported by the
 *        functions of dir.c and fat.c files. file.c uses these functions
 *        heavily, but is not used by fs.c (not true anymore)
 *        partitions and read/write functions to partitions.
 *
 */
#ifndef __FS_H_
#define __FS_H_

#include <stdint.h>
#include <stdbool.h>
#include "efs-cfg.h"
#include "error.h"
#include "partition.h"
#include "debug.h"
#include "time.h"
#include "extract.h"

#define FAT12 1
#define FAT16 2
#define FAT32 3

#define FS_INFO_SECTOR 1
#define FSINFO_MAGIC_BEGIN 0x41615252
#define FSINFO_MAGIC_END   0xAA550000

/*****************************************************************************************\
              VolumeId
               ------
* ushort BytesPerSector		Must be 512 or shit happens.
* uchar  SectorsPerCluster	Must be multiple of 2 (1,2,4,8,16 or 32)
* ushort ReservedSectorCount	Number of sectors after which the first FAT begins.
* uchar	 NumberOfFats		Should be 2
* ushort RootEntryCount		Number of filerecords the Rootdir can contain. NOT for FAT32
* ushort SectorCount16		Number of Sectors for 12/16 bit FAT
* ushort FatSectorCount16	Number of Sectors for 1 FAT on FAT12/16 bit FAT's
* ulong  SectorCount32		Number of Sectors for 32 bit FAT
* ulong FatSectorCount32	Number of Sectors for 1 FAT on FAT32
* ulong RootCluster			Clusternumber of the first cluster of the RootDir on FAT 32
This is NOT a complete volumeId copy, no direct I/O is possible.
\*****************************************************************************************/
struct _VolumeId{
	uint16_t BytesPerSector;
	uint8_t SectorsPerCluster;
	uint16_t ReservedSectorCount;
	uint8_t NumberOfFats;
	uint16_t RootEntryCount;
	uint16_t SectorCount16;
	uint16_t FatSectorCount16;
	uint32_t SectorCount32;
	uint32_t FatSectorCount32;
	uint32_t RootCluster;
};
typedef struct _VolumeId VolumeId;

/**************************************************************************************************\
              FileSystem
               --------
* Partition* 	part		Pointer to partition on which this FS resides.
* VolumeId		volumeId	Contains important FS info.
* ulong			DataClusterCount	Number of dataclusters. This number determines the FATType.
* ulong			FatSectorCount		Number of sectors for 1 FAT, regardless of FATType
* ulong			SectorCount			Number of sectors, regardless of FATType
* ulong 		FirstSectorRootDir	First sector of the RootDir.
* uchar			type				Determines FATType (FAT12 FAT16 or FAT32 are defined)

\**************************************************************************************************/
struct _FileSystem{
	Partition *part;
	VolumeId volumeId;
	uint32_t DataClusterCount;
	uint32_t FatSectorCount;
	uint32_t SectorCount;
	uint32_t FirstSectorRootDir;
	uint32_t FirstClusterCurrentDir;
	uint32_t FreeClusterCount;
	uint32_t NextFreeCluster;
	uint8_t type;
};
typedef struct _FileSystem FileSystem;

/**************************************************************************************************\              FileLocation
               ----------
* euint32		Sector				Sector where the directoryentry of the file/directory can be found.
* euint8		Offset				Offset (in 32byte segments) where in the sector the entry is.

\**************************************************************************************************/
struct _FileLocation{
	uint32_t Sector;
	uint8_t Offset;
	uint8_t attrib;
};
typedef struct _FileLocation FileLocation;

/*****************************************************************************\
*                               FileCache
*                              -----------
* This struct acts as cache for the current file. It contains the current
* FATPointer (next location in the FAT table), LogicCluster
* (the last part of the file that was read) and DataCluster
* (the last cluster that was read).
* euint8		Linear				For how many more clusters the file is nonfragmented
* euint32		LogicCluster		This field determines the n'th cluster of the file as current
* euint32		DiscCluster		If this field is 0, it means the cache is invalid. Otherwise
									it is the clusternumber corresponding with
									logic(FirstCluster+LogicCluster).
* euint32		FirstCluster		First cluster of the chain. Zero or one are invalid.
* euint32		LastCluster		Last cluster of the chain (is not always filled in)
\*****************************************************************************/
struct _ClusterChain{
	uint8_t Linear;
	int32_t LogicCluster;
	uint32_t DiscCluster;
	uint32_t FirstCluster;
	uint32_t LastCluster;
};
typedef struct _ClusterChain ClusterChain;

/*****************************************************************************\
*                               FileRecord                                    *
*                              ------------                                   *
* This struct represents a 32 byte file entry as it occurs in the data area   *
* of the filesystem. Direct I/O is possible.                                  *
\*****************************************************************************/
struct _FileRecord{
	uint8_t FileName[11];
	uint8_t Attribute;
	uint8_t NTReserved;
	uint8_t MilliSecTimeStamp;
	uint16_t CreatedTime;
	uint16_t CreatedDate;
	uint16_t AccessDate;
	uint16_t FirstClusterHigh;
	uint16_t WriteTime;
	uint16_t WriteDate;
	uint16_t FirstClusterLow;
	uint32_t FileSize;
};
typedef struct _FileRecord FileRecord;


int16_t fs_initFs(FileSystem *fs,Partition *part);
int16_t fs_isValidFat(Partition *part);
void fs_loadVolumeId(FileSystem *fs, Partition *part);
int16_t fs_verifySanity(FileSystem *fs);
void fs_countDataSectors(FileSystem *fs);
void fs_determineFatType(FileSystem *fs);
void fs_findFirstSectorRootDir(FileSystem *fs);
void fs_initCurrentDir(FileSystem *fs);
uint32_t fs_getSectorAddressRootDir(FileSystem *fs,uint32_t secref);
uint32_t fs_clusterToSector(FileSystem *fs,uint32_t cluster);
uint32_t fs_sectorToCluster(FileSystem *fs,uint32_t sector);
uint32_t fs_getNextFreeCluster(FileSystem *fs,uint32_t startingcluster);
uint32_t fs_giveFreeClusterHint(FileSystem *fs);
int16_t fs_findFreeFile(FileSystem *fs,uint8_t* filename,FileLocation *loc,uint8_t mode);
int8_t fs_findFile(FileSystem *fs,uint8_t* filename,FileLocation *loc,uint32_t *lastDir);
int8_t fs_findFile_broken(FileSystem *fs,uint8_t* filename,FileLocation *loc);
uint32_t fs_getLastCluster(FileSystem *fs,ClusterChain *Cache);
uint32_t fs_getFirstClusterRootDir(FileSystem *fs);
uint16_t fs_makeDate(void);
uint16_t fs_makeTime(void);
void fs_setFirstClusterInDirEntry(FileRecord *rec,uint32_t cluster_addr);
void fs_initClusterChain(FileSystem *fs,ClusterChain *cache,uint32_t cluster_addr);
int8_t fs_flushFs(FileSystem *fs);
int8_t fs_umount(FileSystem *fs);
int8_t fs_clearCluster(FileSystem *fs,uint32_t cluster);
int8_t fs_getFsInfo(FileSystem *fs);
int8_t fs_setFsInfo(FileSystem *fs);

#endif
