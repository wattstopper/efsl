/*****************************************************************************\
*                     EFSL - Embedded Filesystems Library                     *
*                     -----------------------------------                     *
*                                                                             *
* Filename : fat.h                                                            *
* Release  : 0.3 - devel                                                      *
* Description : This file contains all the functions dealing with the FAT     *
*               in a Microsoft FAT filesystem. It belongs under fs.c          *
*                                                                             *
* This program is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU General Public License                 *
* as published by the Free Software Foundation; version 2                     *
* of the License.                                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
* GNU General Public License for more details.                                *
*                                                                             *
* As a special exception, if other files instantiate templates or             *
* use macros or inline functions from this file, or you compile this          *
* file and link it with other works to produce a work based on this file,     *
* this file does not by itself cause the resulting work to be covered         *
* by the GNU General Public License. However the source code for this         *
* file must still be made available in accordance with section (3) of         *
* the GNU General Public License.                                             *
*                                                                             *
* This exception does not invalidate any other reasons why a work based       *
* on this file might be covered by the GNU General Public License.            *
*                                                                             *
*                                                    (c)2006 Lennart Yseboodt *
*                                                    (c)2006 Michael De Nil   *
\*****************************************************************************/

#ifndef __FAT_H_
#define __FAT_H_

/*****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "efs-cfg.h"
#include "error.h"
#include "file.h"
#include "debug.h"
/*****************************************************************************/

uint32_t fat_getSectorAddressFatEntry(FileSystem *fs,uint32_t cluster_addr);
uint32_t fat_getNextClusterAddress(FileSystem *fs,	uint32_t cluster_addr, uint16_t *linear);
void fat_setNextClusterAddress(FileSystem *fs,uint32_t cluster_addr,uint32_t next_cluster_addr);
bool fat_isEocMarker(FileSystem *fs,uint32_t fat_entry);
uint32_t fat_giveEocMarker(FileSystem *fs);
uint32_t fat_findClusterAddress(FileSystem *fs,uint32_t cluster,uint32_t offset, uint8_t *linear);
uint32_t fat_getNextClusterAddressWBuf(FileSystem *fs,uint32_t cluster_addr, uint8_t * buf);
void fat_setNextClusterAddressWBuf(FileSystem *fs,uint32_t cluster_addr,uint32_t next_cluster_addr,uint8_t * buf);
int16_t fat_getNextClusterChain(FileSystem *fs, ClusterChain *Cache);
void fat_bogus(void);
int16_t fat_LogicToDiscCluster(FileSystem *fs, ClusterChain *Cache,uint32_t logiccluster);
int16_t fat_allocClusterChain(FileSystem *fs,ClusterChain *Cache,uint32_t num_clusters);
int16_t fat_unlinkClusterChain(FileSystem *fs,ClusterChain *Cache);
uint32_t fat_countClustersInChain(FileSystem *fs,uint32_t firstcluster);
uint32_t fat_DiscToLogicCluster(FileSystem *fs,uint32_t firstcluster,uint32_t disccluster);

#endif
