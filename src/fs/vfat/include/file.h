/*****************************************************************************\
*                     EFSL - Embedded Filesystems Library                     *
*                     -----------------------------------                     *
*                                                                             *
* Filename : file.h                                                           *
* Release  : 0.3 - devel                                                      *
* Description : This file contains functions dealing with files such as:      *
*               fopen, fread and fwrite.                                      *
*                                                                             *
* This program is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU General Public License                 *
* as published by the Free Software Foundation; version 2                     *
* of the License.                                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
* GNU General Public License for more details.                                *
*                                                                             *
* As a special exception, if other files instantiate templates or             *
* use macros or inline functions from this file, or you compile this          *
* file and link it with other works to produce a work based on this file,     *
* this file does not by itself cause the resulting work to be covered         *
* by the GNU General Public License. However the source code for this         *
* file must still be made available in accordance with section (3) of         *
* the GNU General Public License.                                             *
*                                                                             *
* This exception does not invalidate any other reasons why a work based       *
* on this file might be covered by the GNU General Public License.            *
*                                                                             *
*                                                    (c)2006 Lennart Yseboodt *
*                                                    (c)2006 Michael De Nil   *
\*****************************************************************************/

#ifndef __FILE_H_
#define __FILE_H_

/*****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "efs-cfg.h"
#include "error.h"
#include "time.h"
#include "fs.h"
#include "dir.h"
#include "plibc.h"
#include "debug.h"
#include "fat.h"
/*****************************************************************************/

#define MODE_READ 0x72
#define MODE_WRITE 0x77
#define MODE_APPEND 0x61

#define FILE_STATUS_OPEN 0
#define FILE_STATUS_WRITE 1

/*****************************************************************************\
 *                              File
 *                             ------
 * FileRecord		DirEntry		Copy of the FileRecord for this file
 * FileLocation	Location		Location of the direntry
 * FileSystem		*fs				Pointer to the filesystem this file is on
 * FileCache		Cache			Pointer to the cache object of the file
 * euint8			FileStatus		Contains bitfield regarding filestatus
 * euint32			FilePtr			Offsetpointer for fread/fwrite functions
 * euint32 		FileSize		Working copy of the filesize, always use this,
 									it is more up to date than DirEntry->FileSize,
									which is only updated when flushing to disc.
\*****************************************************************************/
struct _File{
	FileRecord DirEntry;
	FileLocation Location; /* Location in directory!! */
	FileSystem *fs;
	ClusterChain Cache;
	uint8_t	FileStatus;
	uint32_t FilePtr;
	uint32_t FileSize;
};
typedef struct _File File;


int8_t file_fopen(File *file, FileSystem *fs,uint8_t *filename, uint8_t mode);
int8_t file_fclose(File *file);
int16_t file_setpos(File *file,uint32_t pos);
uint32_t file_fread(File *file,uint32_t offset, uint32_t size,uint8_t *buf);
uint32_t file_read (File *file,uint32_t size,uint8_t *buf);
uint32_t file_fwrite(File* file,uint32_t offset,uint32_t size,uint8_t* buf);
uint32_t file_write (File* file,uint32_t size,uint8_t* buf);
uint8_t* file_normalToFatName(uint8_t* filename,uint8_t* fatfilename);
uint8_t file_validateChar(uint8_t c);
void file_initFile(File *file, FileSystem *fs, FileLocation *loc);
int16_t file_allocClusterChain(File *file,uint32_t num_clusters);
void file_setAttr(File* file,uint8_t attribute,uint8_t val);
uint8_t file_getAttr(File* file,uint8_t attribute);
uint32_t file_requiredCluster(File *file,uint32_t offset, uint32_t size);

#endif
