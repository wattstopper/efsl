/**
 * @file
 * @author Lennart Yseboodt
 * @author Michael De Nil
 * @date 2006
 * @brief These are general filesystem functions, supported by the
 *        functions of dir.c and fat.c files. file.c uses these functions
 *        heavily, but is not used by fs.c (not true anymore)
 *        partitions and read/write functions to partitions.
 *
 * @section LICENSE
 *
 * EFSL - Embedded Filesystems Library
 * -----------------------------------
 * (c)2006 Lennart Yseboodt
 * (c)2006 Michael De Nil
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As a special exception, if other files instantiate templates or
 * use macros or inline functions from this file, or you compile this
 * file and link it with other works to produce a work based on this file,
 * this file does not by itself cause the resulting work to be covered
 * by the GNU General Public License. However the source code for this
 * file must still be made available in accordance with section (3) of
 * the GNU General Public License.
 *
 * This exception does not invalidate any other reasons why a work based
 * on this file might be covered by the GNU General Public License.
 */
#include <stdint.h>
#include <stdbool.h>
#include "fs.h"
#include "fat.h"
#include "dir.h"

/**
 * This functions glues the initialisation of the filesystem together.
 * It loads the volumeID, computes the FS type and searches for the rootsector.
 *
 * @param fs #FileSystem structure on which to operate
 * @param part #Partition structure on which to operate
 *
 * @return Returns 0 on success and -1 on error (if magic code is wrong)
 */
int16_t fs_initFs(FileSystem *fs,Partition *part)
{
	if(!fs_isValidFat(part)){
		return(-1);
	}
	fs->part=part;
	fs_loadVolumeId(fs,part);
	if(!fs_verifySanity(fs))return(-2);
  	fs_countDataSectors(fs);
	fs_determineFatType(fs);
	fs_findFirstSectorRootDir(fs);
	fs_initCurrentDir(fs);

	return(0);
}

/**
 * This functions loads the volumeID and checks if the magic
 * value is present.
 *
 * @return 0 when magic code is missing, 1 if it is there.
*/
int16_t fs_isValidFat(Partition *part)
{
	uint8_t *buf;

	buf=part_getSect(part,0,IOM_MODE_READONLY|IOM_MODE_EXP_REQ); /* Load Volume label */
	if( ex_getb16(buf+0x1FE) != 0xAA55 ){
		return (0);
	}
	part_relSect(part,buf);
	return(1);
}


/**
 * This function loads all relevant fields from the volumeid.
 *
 * @param fs - #FileSystem to load the fields into
 * @param part - #Partition to load from
 */
void fs_loadVolumeId(FileSystem *fs, Partition *part)
{
	uint8_t *buf;

	buf=part_getSect(part,0,IOM_MODE_READONLY|IOM_MODE_EXP_REQ);

	fs->volumeId.BytesPerSector=ex_getb16(buf+0x0B);
	fs->volumeId.SectorsPerCluster=*((uint8_t*)(buf+0x0D));
	fs->volumeId.ReservedSectorCount=ex_getb16(buf+0x0E);
	fs->volumeId.NumberOfFats=*((uint8_t*)(buf+0x10));
	fs->volumeId.RootEntryCount=ex_getb16(buf+0x11);
	fs->volumeId.SectorCount16=ex_getb16(buf+0x13);
	fs->volumeId.FatSectorCount16=ex_getb16(buf+0x16);
	fs->volumeId.SectorCount32=ex_getb32(buf+0x20);
	fs->volumeId.FatSectorCount32=ex_getb32(buf+0x24);
	fs->volumeId.RootCluster=ex_getb32(buf+0x2C);

	part_relSect(part,buf);

}

/**
 * Does some sanity calculations on a filesystem
 *
 * @param fs - #FileSystem to check sanity
 *
 * @return 1 on success, 0 when discrepancies were found.
*/
int16_t fs_verifySanity(FileSystem *fs)
{
	int16_t sane=1; /* Sane until proven otherwise */
	/* First check, BPS, we only support 512 */
	if(fs->volumeId.BytesPerSector!=512)sane=0;
	/* Check is SPC is valid (multiple of 2, and clustersize >=32KB */
	if(!((fs->volumeId.SectorsPerCluster == 1 ) |
	     (fs->volumeId.SectorsPerCluster == 2 ) |
	     (fs->volumeId.SectorsPerCluster == 4 ) |
	     (fs->volumeId.SectorsPerCluster == 8 ) |
	     (fs->volumeId.SectorsPerCluster == 16) |
	     (fs->volumeId.SectorsPerCluster == 32) |
	     (fs->volumeId.SectorsPerCluster == 64) ))sane=0;
	/* Any number of FAT's should be supported... (untested) */
	/* There should be at least 1 reserved sector */
	if(fs->volumeId.ReservedSectorCount==0)sane=0;
	if(fs->volumeId.FatSectorCount16 != 0){
		if(fs->volumeId.FatSectorCount16 > fs->part->disc->partitions[fs->part->activePartition].numSectors)sane=0;
	}else{
		if(fs->volumeId.FatSectorCount32 > fs->part->disc->partitions[fs->part->activePartition].numSectors)sane=0;
	}
	return(sane);
}

/**
 * This functions calculates the sectorcounts, fatsectorcounts and
 * dataclustercounts. It fills in the general fields.
 *
 * @param fs - #FileSystem to use to update its fields
 */
void fs_countDataSectors(FileSystem *fs)
{
  uint32_t rootDirSectors,dataSectorCount;

  rootDirSectors=((fs->volumeId.RootEntryCount*32) +
                 (fs->volumeId.BytesPerSector - 1)) /
                 fs->volumeId.BytesPerSector;

  if(fs->volumeId.FatSectorCount16 != 0)
  {
    fs->FatSectorCount=fs->volumeId.FatSectorCount16;
    fs->volumeId.FatSectorCount32=0;
  }
  else
  {
    fs->FatSectorCount=fs->volumeId.FatSectorCount32;
    fs->volumeId.FatSectorCount16=0;
  }

  if(fs->volumeId.SectorCount16!=0)
  {
    fs->SectorCount=fs->volumeId.SectorCount16;
    fs->volumeId.SectorCount32=0;
  }
  else
  {
    fs->SectorCount=fs->volumeId.SectorCount32;
    fs->volumeId.SectorCount16=0;
  }

  dataSectorCount=fs->SectorCount - (
                  fs->volumeId.ReservedSectorCount +
                  (fs->volumeId.NumberOfFats * fs->FatSectorCount) +
                  rootDirSectors);

  fs->DataClusterCount=dataSectorCount/fs->volumeId.SectorsPerCluster;
}

/**
 * This function looks af the Dataclustercount and determines the
 * FAT type. It fills in fs->type.
 *
 * @param fs - #FileSystem to use to determine FAT type
 */
void fs_determineFatType(FileSystem *fs)
{
	if(fs->DataClusterCount < 4085)
	{
		fs->type=FAT12;
		fs->volumeId.RootCluster=0;
	}
	else if(fs->DataClusterCount < 65525)
	{
		fs->type=FAT16;
		fs->volumeId.RootCluster=0;
	}
	else
	{
		fs->type=FAT32;
	}
}

/**
 * This functions fills in the fs->FirstSectorRootDir field, even
 * for FAT32, although that is not necessary
 * (because you have FirstClusterRootDir).
 *
 * @param fs - #FileSystem to seek the root
 */
void fs_findFirstSectorRootDir(FileSystem *fs)
{
	if(fs->type==FAT32)
		fs->FirstSectorRootDir = fs->volumeId.ReservedSectorCount +
		                         (fs->volumeId.NumberOfFats*fs->volumeId.FatSectorCount32) +
								 (fs->volumeId.RootCluster-2)*fs->volumeId.SectorsPerCluster;
	else
		fs->FirstSectorRootDir = fs->volumeId.ReservedSectorCount +
		                         (fs->volumeId.NumberOfFats*fs->volumeId.FatSectorCount16);
}

void fs_initCurrentDir(FileSystem *fs)
{
	fs->FirstClusterCurrentDir = fs_getFirstClusterRootDir(fs);
}

/**
 * This function converts a clusternumber in the effective sector
 * number where this cluster starts. Boundary check is not implemented
 *
 * @param fs - #FileSystem file system data
 * @param cluster - a contiguous area of disk storage that is numbered
 *
 * @return A 32-bit value representing the sectornumber.
 */
uint32_t fs_clusterToSector(FileSystem *fs,uint32_t cluster)
{
	uint32_t base;

	if(fs->type==FAT32)
	{
		base=
			fs->volumeId.ReservedSectorCount+
			fs->FatSectorCount*fs->volumeId.NumberOfFats;
	}
	else
	{
		base=
			fs->volumeId.ReservedSectorCount+
			fs->FatSectorCount*fs->volumeId.NumberOfFats+
			fs->volumeId.RootEntryCount/16;
	}
	return( base + (cluster-2)*fs->volumeId.SectorsPerCluster );
}

/**
 * Determines a cluster given a sector
 *
 * @param fs - #FileSystem file system data
 * @param sector - smallest storage unit that is addressable by a hard drive
 *
 * @return A 32-bit cluster number.
 */
uint32_t fs_sectorToCluster(FileSystem *fs,uint32_t sector)
{
	uint32_t base;

	if(fs->type==FAT32)
	{
		base=
			fs->volumeId.ReservedSectorCount+
			fs->FatSectorCount*fs->volumeId.NumberOfFats;
	}
	else
	{
		base=
			fs->volumeId.ReservedSectorCount+
			fs->FatSectorCount*fs->volumeId.NumberOfFats+
			fs->volumeId.RootEntryCount/16;
	}
	return(((sector-base)-((sector-base)%fs->volumeId.SectorsPerCluster))/fs->volumeId.SectorsPerCluster+2 );
}


/**
 * This functions searches for a free cluster, starting it's search at
 * cluster startingcluster. This allow to speed up searches and try to avoid
 * fragmentation. Implementing rollover search is still to be done.
 *
 * @param fs - #FileSystem file system data
 * @param startingcluster - a contiguous area of disk storage that is numbered
 *
 * @return If a free cluster is found it's number is returned. If none is
 * found 0 is returned.
*/
uint32_t fs_getNextFreeCluster(FileSystem *fs,uint32_t startingcluster)
{
	uint32_t r;

	while(startingcluster<fs->DataClusterCount){
		r=fat_getNextClusterAddress(fs,startingcluster,0);
		if(r==0){
			return(startingcluster);
		}
		startingcluster++;
	}
	return(0);
}

/**
 * This function should return a clusternumber that is free or
 * lies close before free clusters. The result MUST be checked to see if
 * it is free! Implementationhint: search the largest clusternumber in the
 * files in the rootdirectory.
 *
 * @param fs - #FileSystem file system data
 *
 * @return a best guess at a free cluster number
 */
uint32_t fs_giveFreeClusterHint(FileSystem *fs)
{
	return(2); /* Now THIS is a hint ;) */
}


/**
 * This function looks if the given filename is on the given fs
 * and, if found, fills in its location in loc.
 * The function will first check if the pathname starts with a slash. If so it will
 * set the starting directory to the rootdirectory. Else, it will take the firstcluster-
 * currentdir (That you can change with chdir()) as startingpoint.
 * The lastdir pointer will be the first cluster of the last directory fs_findfile
 * enters. It starts out at the root/current dir and then traverses the path along with
 * fs_findFile.
 * It is set to 0 in case of errors (like dir/dir/dir/file/dir/dir...)
 *
 * @param fs - #FileSystem file system data
 * @param filename - the name of the file
 * @param loc - the location that is filled in after filename is found
 * @param lastDir - first cluster of the last directory entered, or 0 on error
 *
 * @return Returns 0 when nothing was found, 1 when the thing found
 * was a file and 2 if the thing found was a directory.
 */
int8_t fs_findFile(FileSystem *fs,uint8_t* filename,FileLocation *loc,uint32_t *lastDir)
{
	uint32_t fccd,tmpclus;
	uint8_t ffname[11],*next,it=0,filefound=0;

	if(*filename=='/'){
		fccd = fs_getFirstClusterRootDir(fs);
		filename++;
		if(!*filename){
			if(lastDir)*lastDir=fccd;
			return(2);
		}
	}else{
		fccd = fs->FirstClusterCurrentDir;
	}

	if(lastDir)*lastDir=fccd;

	while((next=file_normalToFatName(filename,ffname))!=0){
		if((tmpclus=dir_findinDir(fs,ffname,fccd,loc,DIRFIND_FILE))==0)return(0);
		it++;
		if(loc->attrib&ATTR_DIRECTORY){
			fccd = tmpclus;
			filename = next;
			if(lastDir)*lastDir=fccd;
			if(filefound)*lastDir=0;
		}else{
			filefound=1;
			if((file_normalToFatName(next,ffname))!=0){
				return(0);
			}else{
				filename=next;
			}
		}
	}

	if(it==0)return(0);
	if(loc->attrib&ATTR_DIRECTORY || !filefound)return(2);
	return(1);
}

/**
 * This function seeks to find a free file
 *
 * @param fs - #FileSystem file system data
 * @param filename - the name of the file
 * @param loc - the location that is filled in after filename is found
 * @param mode - unused
 *
 * @return Returns 0 when file was found and is not free, 1 when a file is free
 */
int16_t fs_findFreeFile(FileSystem *fs,uint8_t* filename,FileLocation *loc,uint8_t mode)
{
	uint32_t targetdir=0;
	uint8_t ffname[11];

	if(fs_findFile(fs,filename,loc,&targetdir))return(0);
	if(!dir_getFatFileName(filename,ffname))return(0);
	if(dir_findinDir(fs,ffname,targetdir,loc,DIRFIND_FREE)){
		return(1);
	}else{
		if(dir_addCluster(fs,targetdir)){
			return(0);
		}else{
			if(dir_findinDir(fs,ffname,targetdir,loc,DIRFIND_FREE)){
				return(1);
			}
		}
	}

	return(0);
}

/**
 * This function searches the last cluster of a chain.
 *
 * @param fs - #FileSystem file system data
 * @param Cache - #ClusterChain cache
 *
 * @return The LastCluster (also stored in cache);
 */
uint32_t fs_getLastCluster(FileSystem *fs,ClusterChain *Cache)
{
	if(Cache->DiscCluster==0){
		Cache->DiscCluster=Cache->FirstCluster;
		Cache->LogicCluster=0;
	}

	if(Cache->LastCluster==0)
	{
		while(fat_getNextClusterChain(fs, Cache)==0)
		{
			Cache->LogicCluster+=Cache->Linear;
			Cache->DiscCluster+=Cache->Linear;
			Cache->Linear=0;
		}
	}
	return(Cache->LastCluster);
}

/**
 * This function searches for the root cluster
 *
 * @param fs - #FileSystem file system data
 *
 * @return The RootCluster
 */
uint32_t fs_getFirstClusterRootDir(FileSystem *fs)
{
	switch(fs->type){
		case FAT32:
			return(fs->volumeId.RootCluster);
			break;
		default:
				return(1);
				break;
	}
}

/**
 * This function initializes the cluster chain cache
 *
 * @param fs - #FileSystem file system data
 * @param Cache - #ClusterChain cache to be initialized
 * @param cluster_addr - address of the First Cluster
 */
void fs_initClusterChain(FileSystem *fs,ClusterChain *cache,uint32_t cluster_addr)
{
	cache->FirstCluster=cluster_addr;
	cache->DiscCluster=cluster_addr;
	cache->LogicCluster=0;
	cache->LastCluster=0; /* Warning flag here */
	cache->Linear=0;
}

/**
 * This function initializes the first cluster entry in a Directory Entry
 *
 * @param fs - #FileRecord file record
 * @param cluster_addr - address of the cluster for the entry
 */
void fs_setFirstClusterInDirEntry(FileRecord *rec,uint32_t cluster_addr)
{
	rec->FirstClusterHigh=cluster_addr>>16;
	rec->FirstClusterLow=cluster_addr&0xFFFF;
}

/**
 * This function flushes the filesystem
 *
 * @param fs - #FileRecord file record
 *
 * @return whatever part_flushPart returns
 */
int8_t fs_flushFs(FileSystem *fs)
{
	return(part_flushPart(fs->part,0,fs->SectorCount));
}

/**
 * This function unmounts the filesystem
 *
 * @param fs - #FileRecord file record
 *
 * @return whatever fs_flushFs returns
 */
int8_t fs_umount(FileSystem *fs)
{
	return(fs_flushFs(fs));
}

/**
 * This function clears a cluster in the filesystem
 *
 * @param fs - #FileSystem file system data
 * @param cluster - a contiguous area of disk storage that is numbered
 *
 * @return always returns 0
 */
int8_t fs_clearCluster(FileSystem *fs,uint32_t cluster)
{
	uint16_t c;
	uint8_t* buf;

	for(c=0;c<(fs->volumeId.SectorsPerCluster);c++){
		buf = part_getSect(fs->part,fs_clusterToSector(fs,cluster)+c,IOM_MODE_READWRITE);
		memClr(buf,512);
		part_relSect(fs->part,buf);
	}
	return(0);
}

/**
 * This function gets and stores info about the underlying filesystem
 *
 * @param fs - #FileSystem file system data
 *
 * @return When successful, returns 0.
 *         When not able to get valid info, returns -1.
*/
int8_t fs_getFsInfo(FileSystem *fs)
{
	uint8_t *buf;

 	if(!fs->type==FAT32)return(0);
	buf = part_getSect(fs->part,FS_INFO_SECTOR,IOM_MODE_READONLY);
	if(ex_getb32(buf+0)!=FSINFO_MAGIC_BEGIN || ex_getb32(buf+508)!=FSINFO_MAGIC_END){
		part_relSect(fs->part,buf);
		return(-1);
	}
	fs->FreeClusterCount = ex_getb32(buf+488);
	fs->NextFreeCluster  = ex_getb32(buf+492);
	part_relSect(fs->part,buf);
	return(0);
}

/**
 * This function sets and stores info about the underlying filesystem
 *
 * @param fs - #FileSystem file system data
 *
 * @return When successful, returns 0.
 *         When not able to get valid info, returns -1.
*/
int8_t fs_setFsInfo(FileSystem *fs)
{
	uint8_t* buf;

	if(!fs->type==FAT32)return(0);
	buf = part_getSect(fs->part,FS_INFO_SECTOR,IOM_MODE_READWRITE);
	if(ex_getb32(buf+0)!=FSINFO_MAGIC_BEGIN || ex_getb32(buf+508)!=FSINFO_MAGIC_END){
		part_relSect(fs->part,buf);
		return(-1);
	}
	ex_setb32(buf+488,fs->FreeClusterCount);
	ex_setb32(buf+492,fs->NextFreeCluster);
	part_relSect(fs->part,buf);
	return(0);
}

