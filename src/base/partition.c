/**
 * @file
 * @author Lennart Yseboodt
 * @author Michael De Nil
 * @date 2006
 * @brief These functions are partition specific. Searching FAT type
 *        partitions and read/write functions to partitions.
 *
 * @section LICENSE
 *
 * EFSL - Embedded Filesystems Library
 * -----------------------------------
 * (c)2006 Lennart Yseboodt
 * (c)2006 Michael De Nil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * As a special exception, if other files instantiate templates or
 * use macros or inline functions from this file, or you compile this
 * file and link it with other works to produce a work based on this file,
 * this file does not by itself cause the resulting work to be covered
 * by the GNU General Public License. However the source code for this
 * file must still be made available in accordance with section (3) of
 * the GNU General Public License.
 *
 * This exception does not invalidate any other reasons why a work based
 * on this file might be covered by the GNU General Public License.
 */
#include <stdint.h>
#include <stdbool.h>
#include "partition.h"

/**
 * This function searches the 4 partitions for a FAT class partition
 * and marks the first one found as the active to be used partition.
 *
 * @param part - #Partition structure
 * @param refDisc - #Disc structure
 */
void part_initPartition(Partition *part,Disc* refDisc)
{
	int16_t c;

	part->disc=refDisc;
	part->activePartition=-1; /* No partition selected */
	part_setError(part,PART_NOERROR);
	for(c=3;c>=0;c--){
		if(part_isFatPart(part->disc->partitions[c].type))
			part->activePartition=c;
	}
}

/**
 * This functions checks if a partitiontype is of the FAT
 * type in the broadest sense.
 *
 * @param type - partition type
 *
 * @return If it is FAT, returns true, otherwise false.
 */
bool part_isFatPart(uint8_t type)
{
	if(type == PT_FAT12  ||
	   type == PT_FAT16A ||
	   type == PT_FAT16  ||
	   type == PT_FAT32  ||
	   type == PT_FAT32A ||
	   type == PT_FAT16B   )
	{
		return true;
	}
	return false;
}

/**
 * This functions reads a 512 byte partition into a buffer.
 * Its offset is address sectors from the beginning of the partition.
 *
 * @param part - #Partition structure
 * @param address - address offset
 * @param buf - where to store the data that was read
 *
 * @return If it is FAT, returns true, otherwise false.
 */
int8_t part_readBuf(Partition *part, uint32_t address, uint8_t* buf)
{
	return(if_readBuf(part->disc->ioman->iface,part_getRealLBA(part,address), buf));
}

/**
 * This function writes 512 bytes, from buf. It's offset is address
 * sectors from the beginning of the partition.
 *
 * @param part - #Partition structure
 * @param address - address offset
 * @param buf - where to store the data that was read
 *
 *  * @return It returns whatever the hardware function returns. (-1=error)
 */
int8_t part_writeBuf(Partition *part,uint32_t address,uint8_t* buf)
{
	/*DBG((TXT("part_writeBuf :: %li\n"),address));*/
	return(if_writeBuf(part->disc->ioman->iface,part_getRealLBA(part,address),buf));
}

/**
 * This function calculates what the partition offset for
 * a partition is + the address.
 *
 * @return Sector address.
 */
uint32_t part_getRealLBA(Partition *part,uint32_t address)
{
	return(part->disc->partitions[part->activePartition].LBA_begin+address);
}

/**
 * This function calls ioman_getSector, but recalculates the sector
 * address to be partition relative.
 *
 * @return Whatever getSector returns. (pointer or 0)
 */
uint8_t* part_getSect(Partition *part, uint32_t address, uint8_t mode)
{
	return(ioman_getSector(part->disc->ioman,part_getRealLBA(part,address),mode));
}

/**
 * This function calls ioman_releaseSector.
 *
 * @return Whatever releaseSector returns.
 */
int8_t part_relSect(Partition *part, uint8_t* buf)
{
	return(ioman_releaseSector(part->disc->ioman,buf));
}

/**
 * This function calls ioman_flushRange.
 *
 * @return Whatever ioman_flushRange returns.
 */
int8_t part_flushPart(Partition *part,uint32_t addr_l, uint32_t addr_h)
{
	return(
		ioman_flushRange(part->disc->ioman,part_getRealLBA(part,addr_l),part_getRealLBA(part,addr_h))
	);
}

/**
 * This function calls ioman_directSectorRead.
 *
 * @return Whatever ioman_directSectorRead returns.
 */
int8_t part_directSectorRead(Partition *part,uint32_t address, uint8_t* buf)
{
	return(
		ioman_directSectorRead(part->disc->ioman,part_getRealLBA(part,address),buf)
	);
}

/**
 * This function calls ioman_directSectorWrite.
 *
 * @return Whatever ioman_directSectorWrite returns.
 */
int8_t part_directSectorWrite(Partition *part,uint32_t address, uint8_t* buf)
{
	return(
		ioman_directSectorWrite(part->disc->ioman,part_getRealLBA(part,address),buf)
	);
}
