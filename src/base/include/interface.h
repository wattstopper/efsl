/*****************************************************************************\
*                     EFSL - Embedded Filesystems Library                     *
*                     -----------------------------------                     *
*                                                                             *
* Filename : interface.c                                                      *
* Release  : 0.3 - devel                                                      *
* Description : This file defines the general I/O interface functions         *
*               that can be performed on hardware.                            *
*                                                                             *
* This program is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU General Public License                 *
* as published by the Free Software Foundation; version 2                     *
* of the License.                                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
* GNU General Public License for more details.                                *
*                                                                             *
* As a special exception, if other files instantiate templates or             *
* use macros or inline functions from this file, or you compile this          *
* file and link it with other works to produce a work based on this file,     *
* this file does not by itself cause the resulting work to be covered         *
* by the GNU General Public License. However the source code for this         *
* file must still be made available in accordance with section (3) of         *
* the GNU General Public License.                                             *
*                                                                             *
* This exception does not invalidate any other reasons why a work based       *
* on this file might be covered by the GNU General Public License.            *
*                                                                             *
*                                                    (c)2006 Lennart Yseboodt *
*                                                    (c)2006 Michael De Nil   *
\*****************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include "efs-cfg.h"
#include "ioctl.h"

#ifndef MULTIPLE_INTERFACE_SUPPORT
#include HWIFUNC_HEADER
#endif

#ifndef __INTERFACE_H_
#define __INTERFACE_H_

/* If no multiple interfaces are defined, check if the
   hw functions are defines */
#ifndef MULTIPLE_INTERFACE_SUPPORT

 #ifndef HWIFUNC_INIT
  #error "There is no HW_INIT function defined"
 #endif

 #ifndef HWIFUNC_READ
  #error "There is no HW_READ function defined"
 #endif

 #ifndef HWIFUNC_WRITE
  #error "There is no HW_WRITE function defined"
 #endif

#endif

#ifdef MULTIPLE_INTERFACE_SUPPORT
struct _Interface{
	int8_t (*initInterface)(void*);
	int8_t (*readBuf)(void*,uint32_t,uint8_t*);
	int8_t (*writeBuf)(void*,uint32_t,uint8_t*);
	int8_t (*ioctl)(void*,uint16_t,void*);
	void* interface_data;
	uint32_t sectorCount;
	uint8_t flags;
};
typedef struct _Interface Interface;

int8_t if_init(Interface *iface, void* hwData,
                                              int8_t (*initInterface)(void*),
                                              int8_t (*readBuf) (void*, uint32_t,uint8_t*),
                                              int8_t (*writeBuf) (void*, uint32_t,uint8_t*),
					      				      int8_t (*ioctl) (void*,uint16_t, void*));
int8_t if_readBuf(Interface *iface, uint32_t address, uint8_t* buf);
int8_t if_writeBuf(Interface *iface, uint32_t address, uint8_t* buf);
#endif

#endif
