/*****************************************************************************\
*                     EFSL - Embedded Filesystems Library                     *
*                     -----------------------------------                     *
*                                                                             *
* Filename : ioman.c                                                          *
* Release  : 0.3 - devel                                                      *
* Description : The IO Manager receives all requests for sectors in a central *
*               allowing it to make smart decision regarding caching.         *
*               The IOMAN_NUMBUFFER parameter determines how many sectors     *
*               ioman can cache. ioman also supports overallocating and       *
*               backtracking sectors.                                         *
*                                                                             *
* This program is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU General Public License                 *
* as published by the Free Software Foundation; version 2                     *
* of the License.                                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
* GNU General Public License for more details.                                *
*                                                                             *
* As a special exception, if other files instantiate templates or             *
* use macros or inline functions from this file, or you compile this          *
* file and link it with other works to produce a work based on this file,     *
* this file does not by itself cause the resulting work to be covered         *
* by the GNU General Public License. However the source code for this         *
* file must still be made available in accordance with section (3) of         *
* the GNU General Public License.                                             *
*                                                                             *
* This exception does not invalidate any other reasons why a work based       *
* on this file might be covered by the GNU General Public License.            *
*                                                                             *
*                                                    (c)2006 Lennart Yseboodt *
*                                                    (c)2006 Michael De Nil   *
\*****************************************************************************/

#ifndef __IOMAN_H__
#define __IOMAN_H__

/*****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "interface.h"
#include "error.h"
#include "plibc.h"
#include "debug.h"
#include "efs-cfg.h"
/*****************************************************************************/

#define IOMAN_STATUS_ATTR_VALIDDATA  0
#define IOMAN_STATUS_ATTR_USERBUFFER 1
#define IOMAN_STATUS_ATTR_WRITE      2

#define IOM_MODE_READONLY  1
#define IOM_MODE_READWRITE 2
#define IOM_MODE_EXP_REQ   4

struct _IOManStack{
	uint32_t sector;
	uint8_t  status;
	uint8_t  usage;
};
typedef struct _IOManStack IOManStack;

struct _IOManager{
	Interface *iface;

	uint8_t *bufptr;
	uint16_t numbuf;
	uint16_t numit;

	IOMAN_ERR_EUINT8

	IOManStack stack[IOMAN_NUMBUFFER][IOMAN_NUMITERATIONS];

	uint32_t sector[IOMAN_NUMBUFFER];
	uint8_t  status[IOMAN_NUMBUFFER];
	uint8_t  usage[IOMAN_NUMBUFFER];
	uint8_t  reference[IOMAN_NUMBUFFER];
	uint8_t  itptr[IOMAN_NUMBUFFER];
#ifdef IOMAN_DO_MEMALLOC
	uint8_t  cache_mem[IOMAN_NUMBUFFER * 512];
#endif
};
typedef struct _IOManager IOManager;

#define IOBJ ioman

#define ioman_isValid(bp) ioman_getAttr(IOBJ,bp,IOMAN_STATUS_ATTR_VALIDDATA)
#define ioman_isUserBuf(bp) ioman_getAttr(IOBJ,bp,IOMAN_STATUS_ATTR_USERBUFFER)
#define ioman_isWritable(bp) ioman_getAttr(IOBJ,bp,IOMAN_STATUS_ATTR_WRITE)

#define ioman_setValid(bp) ioman_setAttr(IOBJ,bp,IOMAN_STATUS_ATTR_VALIDDATA,1)
#define ioman_setUserBuf(bp) ioman_setAttr(IOBJ,bp,IOMAN_STATUS_ATTR_USERBUFFER,1)
#define ioman_setWritable(bp) ioman_setAttr(IOBJ,bp,IOMAN_STATUS_ATTR_WRITE,1)

#define ioman_setNotValid(bp) ioman_setAttr(IOBJ,bp,IOMAN_STATUS_ATTR_VALIDDATA,0)
#define ioman_setNotUserBuf(bp) ioman_setAttr(IOBJ,bp,IOMAN_STATUS_ATTR_USERBUFFER,0)
#define ioman_setNotWritable(bp) ioman_setAttr(IOBJ,bp,IOMAN_STATUS_ATTR_WRITE,0)

#define ioman_isReqRo(mode)  ((mode)&(IOM_MODE_READONLY))
#define ioman_isReqRw(mode)  ((mode)&(IOM_MODE_READWRITE))
#define ioman_isReqExp(mode) ((mode)&(IOM_MODE_EXP_REQ))

int8_t ioman_init(IOManager *ioman, Interface *iface, uint8_t* bufferarea);
void ioman_reset(IOManager *ioman);
uint8_t* ioman_getBuffer(IOManager *ioman,uint8_t* bufferarea);
void ioman_setAttr(IOManager *ioman,uint16_t bufplace,uint8_t attribute,uint8_t val);
uint8_t ioman_getAttr(IOManager *ioman,uint16_t bufplace,uint8_t attribute);
uint8_t ioman_getUseCnt(IOManager *ioman,uint16_t bufplace);
void ioman_incUseCnt(IOManager *ioman,uint16_t bufplace);
void ioman_decUseCnt(IOManager *ioman,uint16_t bufplace);
void ioman_resetUseCnt(IOManager *ioman,uint16_t bufplace);
uint8_t ioman_getRefCnt(IOManager *ioman,uint16_t bufplace);
void ioman_incRefCnt(IOManager *ioman,uint16_t bufplace);
void ioman_decRefCnt(IOManager *ioman,uint16_t bufplace);
void ioman_resetRefCnt(IOManager *ioman,uint16_t bufplace);
int8_t ioman_pop(IOManager *ioman,uint16_t bufplace);
int8_t ioman_push(IOManager *ioman,uint16_t bufplace);
uint8_t* ioman_getPtr(IOManager *ioman,uint16_t bufplace);
int16_t ioman_getBp(IOManager *ioman,uint8_t* buf);
int8_t ioman_readSector(IOManager *ioman,uint32_t address,uint8_t* buf);
int8_t ioman_writeSector(IOManager *ioman, uint32_t address, uint8_t* buf);
void ioman_resetCacheItem(IOManager *ioman,uint16_t bufplace);
int32_t ioman_findSectorInCache(IOManager *ioman, uint32_t address);
int32_t ioman_findFreeSpot(IOManager *ioman);
int32_t ioman_findUnusedSpot(IOManager *ioman);
int32_t ioman_findOverallocableSpot(IOManager *ioman);
int8_t ioman_putSectorInCache(IOManager *ioman,uint32_t address, uint16_t bufplace);
int8_t ioman_flushSector(IOManager *ioman, uint16_t bufplace);
uint8_t* ioman_getSector(IOManager *ioman,uint32_t address, uint8_t mode);
int8_t ioman_releaseSector(IOManager *ioman,uint8_t* buf);
int8_t ioman_directSectorRead(IOManager *ioman,uint32_t address, uint8_t* buf);
int8_t ioman_directSectorWrite(IOManager *ioman,uint32_t address, uint8_t* buf);
int8_t ioman_flushRange(IOManager *ioman,uint32_t address_low, uint32_t address_high);
int8_t ioman_flushAll(IOManager *ioman);

void ioman_printStatus(IOManager *ioman);

#endif
