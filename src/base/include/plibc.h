/*****************************************************************************\
*                     EFSL - Embedded Filesystems Library                     *
*                     -----------------------------------                     *
*                                                                             *
* Filename : plibc.c                                                          *
* Release  : 0.3 - devel                                                      *
* Description : This file contains replacements of common c library functions *
*                                                                             *
* This program is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU General Public License                 *
* as published by the Free Software Foundation; version 2                     *
* of the License.                                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
* GNU General Public License for more details.                                *
*                                                                             *
* As a special exception, if other files instantiate templates or             *
* use macros or inline functions from this file, or you compile this          *
* file and link it with other works to produce a work based on this file,     *
* this file does not by itself cause the resulting work to be covered         *
* by the GNU General Public License. However the source code for this         *
* file must still be made available in accordance with section (3) of         *
* the GNU General Public License.                                             *
*                                                                             *
* This exception does not invalidate any other reasons why a work based       *
* on this file might be covered by the GNU General Public License.            *
*                                                                             *
*                                                    (c)2006 Lennart Yseboodt *
*                                                    (c)2006 Michael De Nil   *
\*****************************************************************************/

#ifndef __PLIBC_H__
#define __PLIBC_H__

/*****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "debug.h"
#include "efs-cfg.h"
/*****************************************************************************/

uint16_t strMatch(uint8_t* bufa, uint8_t*bufb,uint32_t n);
void memCpy(void* psrc, void* pdest, uint32_t size);
void memClr(void *pdest,uint32_t size);
void memSet(void *pdest,uint32_t size,uint8_t data);


#endif
