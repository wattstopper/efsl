/**
 * @file
 * @author Lennart Yseboodt
 * @author Michael De Nil
 * @date 2006
 * @brief These are general filesystem functions, supported by the
 *        functions of dir.c and fat.c files. file.c uses these functions
 *        heavily, but is not used by fs.c (not true anymore)
 *        partitions and read/write functions to partitions.
 *
 */
#ifndef __PARTITION_H__
#define __PARTITION_H__

#include <stdint.h>
#include <stdbool.h>
#include "efs-cfg.h"
#include "error.h"
#include "interface.h"
#include "disc.h"

#define PT_FAT12  0x01
#define PT_FAT16A 0x04
#define PT_FAT16  0x06
#define PT_FAT32  0x0B
#define PT_FAT32A 0x5C
#define PT_FAT16B 0x5E

/*************************************************************************************\
              Partition
               -------
* Disc*		disc				Pointer to disc containing this partition.
* int8_t	activePartition	 	Array subscript for disc->partitions[activePartition]
\*************************************************************************************/
struct _Partition{
	Disc *disc;
 	int8_t activePartition;
};
typedef struct _Partition Partition;

void part_initPartition(Partition *part,Disc* refDisc);
bool part_isFatPart(uint8_t type);
int8_t part_readBuf(Partition *part, uint32_t address, uint8_t* buf);
int8_t part_readPartBuf(Partition *part, uint32_t address, uint8_t* buf, uint32_t offset, uint16_t len);
int8_t part_writeBuf(Partition *part,uint32_t address,uint8_t* buf);
uint8_t* part_getSect(Partition *part, uint32_t address,uint8_t mode);
int8_t part_relSect(Partition *part, uint8_t* buf);
int8_t part_flushPart(Partition *part,uint32_t addr_l, uint32_t addr_h);
int8_t part_directSectorRead(Partition *part, uint32_t address, uint8_t* buf);
int8_t part_directSectorWrite(Partition *part, uint32_t address, uint8_t* buf);
uint32_t part_getRealLBA(Partition *part,uint32_t address);

#include "extract.h"

#endif
