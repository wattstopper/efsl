/*****************************************************************************\
*                     EFSL - Embedded Filesystems Library                     *
*                     -----------------------------------                     *
*                                                                             *
* Filename : sd.h                                                             *
* Release  : 0.3 - devel                                                      *
* Description : This file contains the functions needed to use efs for        *
*               accessing files on an SD-card.                                *
*                                                                             *
* This program is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU General Public License                 *
* as published by the Free Software Foundation; version 2                     *
* of the License.                                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
* GNU General Public License for more details.                                *
*                                                                             *
* As a special exception, if other files instantiate templates or             *
* use macros or inline functions from this file, or you compile this          *
* file and link it with other works to produce a work based on this file,     *
* this file does not by itself cause the resulting work to be covered         *
* by the GNU General Public License. However the source code for this         *
* file must still be made available in accordance with section (3) of         *
* the GNU General Public License.                                             *
*                                                                             *
* This exception does not invalidate any other reasons why a work based       *
* on this file might be covered by the GNU General Public License.            *
*                                                                             *
*                                                    (c)2006 Lennart Yseboodt *
*                                                    (c)2006 Michael De Nil   *
\*****************************************************************************/

#ifndef __SD_H__
#define __SD_H__
/*****************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include "efs-cfg.h"
#include "debug.h"
/*****************************************************************************/

#define CMDREAD     17
#define CMDWRITE    24
/*****************************************************************************/

struct _SdSpiProtocol
{
	void *spiHwInterface;
	uint8_t (*spiHwInit)(void* spiHwInterface);
	uint8_t (*spiSendByte)(void* spiHwInterface,uint8_t data);
};
typedef struct _SdSpiProtocol SdSpiProtocol;


int8_t  sd_Init(SdSpiProtocol *ssp);
void sd_Command(SdSpiProtocol *ssp,uint8_t cmd, uint16_t paramx, uint16_t paramy);
uint8_t sd_Resp8b(SdSpiProtocol *ssp);
void sd_Resp8bError(SdSpiProtocol *ssp,uint8_t value);
uint16_t sd_Resp16b(SdSpiProtocol *ssp);
int8_t sd_State(SdSpiProtocol *ssp);

int8_t sd_readSector(SdSpiProtocol *ssp,uint32_t address,uint8_t* buf);
int8_t sd_writeSector(SdSpiProtocol *ssp,uint32_t address, uint8_t* buf);
int8_t sd_ioctl(SdSpiProtocol* ssp,uint16_t ctl,void* data);

#endif
