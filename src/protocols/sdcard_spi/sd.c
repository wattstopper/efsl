/*****************************************************************************\
*                     EFSL - Embedded Filesystems Library                     *
*                     -----------------------------------                     *
*                                                                             *
* Filename : sd.c                                                             *
* Release  : 0.3 - devel                                                      *
* Description : This file contains the functions needed to use efs for        *
*               accessing files on an SD-card.                                *
*                                                                             *
* This program is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU General Public License                 *
* as published by the Free Software Foundation; version 2                     *
* of the License.                                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
* GNU General Public License for more details.                                *
*                                                                             *
* As a special exception, if other files instantiate templates or             *
* use macros or inline functions from this file, or you compile this          *
* file and link it with other works to produce a work based on this file,     *
* this file does not by itself cause the resulting work to be covered         *
* by the GNU General Public License. However the source code for this         *
* file must still be made available in accordance with section (3) of         *
* the GNU General Public License.                                             *
*                                                                             *
* This exception does not invalidate any other reasons why a work based       *
* on this file might be covered by the GNU General Public License.            *
*                                                                             *
*                                                    (c)2006 Lennart Yseboodt *
*                                                    (c)2006 Michael De Nil   *
\*****************************************************************************/

/*****************************************************************************/
#include "sd.h"
/*****************************************************************************/

int8_t  sd_Init(SdSpiProtocol *ssp)
{
	int16_t i;
	uint8_t resp;

	/* Init HW-Layer */
	ssp->spiHwInit(ssp->spiHwInterface);

	/* Try to send reset command up to 512 times */
	i=512;
	do{
		sd_Command(ssp,0, 0, 0);
		resp=sd_Resp8b(ssp);
	}
	while(resp!=1 && i--);

	/* If response was not one, find out what went wrong & quit */
	if(resp!=1){
		if(resp==0xff){
			return(-1);
		}
		else{
			sd_Resp8bError(ssp,resp);
			return(-2);
		}
	}

	/* Wait till card is ready initialising (returns 0 on CMD1) */
	/* Try up to 32000 times. */
	i=32000;
	do{
		sd_Command(ssp,1, 0, 0);

		resp=sd_Resp8b(ssp);
		if(resp!=0)
			sd_Resp8bError(ssp,resp);
	}
	while(resp==1 && i--);

	/* If the card is not ready by now, return an error */
	if(resp!=0){
		sd_Resp8bError(ssp,resp);
		return(-3);
	}

	/* Request state, should be 0 */
	if(sd_State(ssp)<0){
		DBG((TXT("Card didn't return the ready state, breaking up...\n")));
		return(-4);
	}

	return(0);
}
/*****************************************************************************/

void sd_Command(SdSpiProtocol *ssp,uint8_t cmd, uint16_t paramx, uint16_t paramy)
{
	ssp->spiSendByte(ssp->spiHwInterface,0xff);

	ssp->spiSendByte(ssp->spiHwInterface,0x40 | cmd);
	ssp->spiSendByte(ssp->spiHwInterface,(uint8_t) (paramx >> 8));/* MSB of x */
	ssp->spiSendByte(ssp->spiHwInterface,(uint8_t) (paramx)); 	 /* LSB of x */
	ssp->spiSendByte(ssp->spiHwInterface,(uint8_t) (paramy >> 8));/* MSB of y */
	ssp->spiSendByte(ssp->spiHwInterface,(uint8_t) (paramy)); 	 /* LSB of y */

	/* Checksum (should be only valid for first command (0) */
	ssp->spiSendByte(ssp->spiHwInterface,0x95);

	/* Eat response, we don't care for now */
	ssp->spiSendByte(ssp->spiHwInterface,0xff);
}
/*****************************************************************************/

uint8_t sd_Resp8b(SdSpiProtocol *ssp)
{
	uint8_t i;
	uint8_t resp;

	/* Respone will come after 1 - 8 pings */
	for(i=0;i<8;i++){
		resp = ssp->spiSendByte(ssp->spiHwInterface,0xff);
		if(resp != 0xff)
			return(resp);
	}

	return(resp);
}
/*****************************************************************************/

uint16_t sd_Resp16b(SdSpiProtocol *ssp)
{
	uint16_t resp;

	resp = ( sd_Resp8b(ssp) << 8 ) & 0xff00;
	resp |= ssp->spiSendByte(ssp->spiHwInterface,0xff);

	return(resp);
}
/*****************************************************************************/

void sd_Resp8bError(SdSpiProtocol *ssp,uint8_t value)
{
	switch(value)
	{
		case 0x40:
			DBG((TXT("Argument out of bounds.\n")));
			break;
		case 0x20:
			DBG((TXT("Address out of bounds.\n")));
			break;
		case 0x10:
			DBG((TXT("Error during erase sequence.\n")));
			break;
		case 0x08:
			DBG((TXT("CRC failed.\n")));
			break;
		case 0x04:
			DBG((TXT("Illegal command.\n")));
			break;
		case 0x02:
			DBG((TXT("Erase reset (see SanDisk docs p5-13).\n")));
			break;
		case 0x01:
			DBG((TXT("Card is initialising.\n")));
			break;
		default:
			DBG((TXT("Unknown error 0x%x (see SanDisk docs p5-13).\n"),value));
			break;
	}
}
/*****************************************************************************/

int8_t sd_State(SdSpiProtocol *ssp)
{
	int16_t value;

	sd_Command(ssp,13, 0, 0);
	value=sd_Resp16b(ssp);

	switch(value)
	{
		case 0x0000:
			return(1);
			break;
		case 0x0001:
			DBG((TXT("Card is Locked.\n")));
			break;
		case 0x0002:
			DBG((TXT("WP Erase Skip, Lock/Unlock Cmd Failed.\n")));
			break;
		case 0x0004:
			DBG((TXT("General / Unknown error -- card broken?.\n")));
			break;
		case 0x0008:
			DBG((TXT("Internal card controller error.\n")));
			break;
		case 0x0010:
			DBG((TXT("Card internal ECC was applied, but failed to correct the data.\n")));
			break;
		case 0x0020:
			DBG((TXT("Write protect violation.\n")));
			break;
		case 0x0040:
			DBG((TXT("An invalid selection, sectors for erase.\n")));
			break;
		case 0x0080:
			DBG((TXT("Out of Range, CSD_Overwrite.\n")));
			break;
		default:
			if(value>0x00FF)
				sd_Resp8bError(ssp,(uint8_t) (value>>8));
			else
				DBG((TXT("Unknown error: 0x%x (see SanDisk docs p5-14).\n"),value));
			break;
	}
	return(-1);
}
/*****************************************************************************/

/* ****************************************************************************
 * WAIT ?? -- FIXME
 * CMDWRITE
 * WAIT
 * CARD RESP
 * WAIT
 * DATA BLOCK OUT
 *      START BLOCK
 *      DATA
 *      CHKS (2B)
 * BUSY...
 */

int8_t sd_writeSector(SdSpiProtocol *ssp,uint32_t address, uint8_t* buf)
{
	uint32_t place;
	uint16_t i;
	uint16_t t=0;

	/*DBG((TXT("Trying to write %u to sector %u.\n"),(void *)&buf,address));*/
	place=512*address;
	sd_Command(ssp,CMDWRITE, (uint16_t) (place >> 16), (uint16_t) place);

	sd_Resp8b(ssp); /* Card response */

	ssp->spiSendByte(ssp->spiHwInterface,0xfe); /* Start block */
	for(i=0;i<512;i++)
		ssp->spiSendByte(ssp->spiHwInterface,buf[i]); /* Send data */
	ssp->spiSendByte(ssp->spiHwInterface,0xff); /* Checksum part 1 */
	ssp->spiSendByte(ssp->spiHwInterface,0xff); /* Checksum part 2 */

	ssp->spiSendByte(ssp->spiHwInterface,0xff);

	while(ssp->spiSendByte(ssp->spiHwInterface,0xff)!=0xff){
		t++;
		/* Removed NOP */
	}
	/*DBG((TXT("Nopp'ed %u times.\n"),t));*/

	return(0);
}
/*****************************************************************************/

/* ****************************************************************************
 * WAIT ?? -- FIXME
 * CMDCMD
 * WAIT
 * CARD RESP
 * WAIT
 * DATA BLOCK IN
 * 		START BLOCK
 * 		DATA
 * 		CHKS (2B)
 */

int8_t sd_readSector(SdSpiProtocol *ssp,uint32_t address, uint8_t* buf)
{
	uint8_t cardresp;
	uint8_t firstblock;
	uint8_t c;
	uint16_t fb_timeout=0xffff;
	uint32_t i;
	uint32_t place;
	const uint16_t len=512; /* FIXME */

	/*DBG((TXT("sd_readSector::Trying to read sector %u and store it at %p.\n"),address,&buf[0]));*/
	place=512*address;
	sd_Command(ssp,CMDREAD, (uint16_t) (place >> 16), (uint16_t) place);

	cardresp=sd_Resp8b(ssp); /* Card response */

	/* Wait for startblock */
	do
		firstblock=sd_Resp8b(ssp);
	while(firstblock==0xff && fb_timeout--);

	if(cardresp!=0x00 || firstblock!=0xfe){
		sd_Resp8bError(ssp,firstblock);
		return(-1);
	}

	for(i=0;i<512;i++){
		c = ssp->spiSendByte(ssp->spiHwInterface,0xff);
		if(i<len)
			buf[i] = c;
	}

	/* Checksum (2 byte) - ignore for now */
	ssp->spiSendByte(ssp->spiHwInterface,0xff);
	ssp->spiSendByte(ssp->spiHwInterface,0xff);

	return(0);
}
/*****************************************************************************/

int8_t sd_ioctl(SdSpiProtocol* ssp,uint16_t ctl,void* data)
{
	return(0);
}
