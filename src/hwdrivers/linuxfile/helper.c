/*****************************************************************************\
*                     EFSL - Embedded Filesystems Library                     *
*                     -----------------------------------                     *
*                                                                             *
* Filename : helper.c                                                         *
* Release  : 0.3 - devel                                                      *
* Description : These functions may NOT BE USED ANYWHERE ! They are helper    *
*               functions for the Linux based developement. They use the GNU  *
*               C library and headers.                                        *
*                                                                             *
* This program is free software; you can redistribute it and/or               *
* modify it under the terms of the GNU General Public License                 *
* as published by the Free Software Foundation; version 2                     *
* of the License.                                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful,             *
* but WITHOUT ANY WARRANTY; without even the implied warranty of              *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
* GNU General Public License for more details.                                *
*                                                                             *
* As a special exception, if other files instantiate templates or             *
* use macros or inline functions from this file, or you compile this          *
* file and link it with other works to produce a work based on this file,     *
* this file does not by itself cause the resulting work to be covered         *
* by the GNU General Public License. However the source code for this         *
* file must still be made available in accordance with section (3) of         *
* the GNU General Public License.                                             *
*                                                                             *
* This exception does not invalidate any other reasons why a work based       *
* on this file might be covered by the GNU General Public License.            *
*                                                                             *
*                                                    (c)2006 Lennart Yseboodt *
*                                                    (c)2006 Michael De Nil   *
\*****************************************************************************/

#include "helper.h"

void* Malloc(size_t size)
{
	void*x;
	
	if((x=malloc(size))==NULL){
		perror("Malloc: ");
		exit(-1);
	}
	return(x);
}
 
void Fopen(FILE **f,uint8_t* filename)
{
	*f=fopen(filename,"r+");
	if(*f==NULL){
		perror("Fopen: ");
		exit(-1);
	}
}

void MMap(uint8_t* filename,void**x,size_t *size)
{
	FILE *tmp;
	int filesize,c;
	
	Fopen(&tmp,filename);
	filesize=getFileSize(tmp);
	*x=Malloc(filesize);
	for(c=0;c<filesize;c++)*((char*)(*x)+c)=fgetc(tmp);
	*size=(size_t)filesize;
	fclose(tmp);
}

int getFileSize(FILE* file)
{
	int c=0;
	
	fseek(file,0,SEEK_END);
	c=ftell(file);
	return(c);
}

void PrintBuf(uint8_t* buf)
{
	uint16_t c,cc;
	
	for(c=0 ; c<32 ; c++){
			printf("\n%4x : ",c*16);
		for(cc=0;cc<16;cc++){
			printf("%2x ",buf[c*16+cc]);
		}
		printf("   ");
		for(cc=0;cc<16;cc++){
			printf("%c",buf[c*16+cc]>=32?buf[c*16+cc]:'*');
		}
	}
	printf("\n");
}
