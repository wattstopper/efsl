/**
  ******************************************************************************
  * @file    sd_stm32.h
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    11/20/2009
  * @brief   Header for sd_stm32.c
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SD_STM32_H
#define __SD_STM32_H

/* Includes ------------------------------------------------------------------*/
#include "../debug.h"
#include "efs-cfg.h"


/* Exported types ------------------------------------------------------------*/
/*************************************************************\
              hwInterface
               ----------
* FILE* 	imagefile		File emulation of hw interface.
* long		sectorCount		Number of sectors on the file.
\*************************************************************/
struct  hwInterface{
	/*FILE 	*imageFile;*/
	long  	sectorCount;
};
typedef struct hwInterface hwInterface;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
int8_t if_initInterface(hwInterface* file,uint8_t* opts);
int8_t if_readBuf(hwInterface* file,uint32_t address,uint8_t* buf);
int8_t if_writeBuf(hwInterface* file,uint32_t address,uint8_t* buf);
int8_t if_setPos(hwInterface* file,uint32_t address);
void if_spiInit(hwInterface *iface);
uint8_t if_spiSend(hwInterface *iface, uint8_t outgoing);


#endif /*__SD_STM32_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
