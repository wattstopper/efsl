/**
 * @file
 * @author  Steve Karg
 * @date    March 25, 2014
 * @brief   EFS SD card driver's low level interface for Fujitsu MB9BF
 *          using the microSD card interface on WattStopper DLM LMPI board.
 */
#include "hardware.h"
#include "mb9bf_mfs.h"
#include "sd.h"
#include "interface.h"

/**
 * Pinout Connections on the LMPI board
 *  NAME   SD-MODE  SPI-MODE MB9BF Peripheral Pin
 * ------- -------- -------- --------------------------------------
 * SD_MISO DATA_OUT DO/MISO  P75/SIN3_0/ADTG_8/INT07_1/MADATA08_0
 * SD_MOSI DATA_IN  DI/MOSI  P76/SOT3_0/TIOA07_2/INT11_2/MADATA11_0
 * SD_CLK  CLK      CLK      P77/SCK3_0/TIOB07_2/INT12_2/MADATA12_0
 * SD_SS   CS       !CS      P78/AIN1_0/TIOA15_0/MADATA13_0
 * SD_CD   CD                P79/BIN1_0/TIOB15_0/INT23_1/MADATA14_0
 * SD_WP   WP                P7A/ZIN1_0/INT24_1/MADATA15_0
 * microSD                   P10/AN00/MCSX7_0
 *
 * microSD card vs SD Card
 * P10 is unconnected in the LMPI SD card hardware version.
 * In the LMPI microSD card version, P10 is connected to Ground.
 * In FW, that pin would be defined as an input and its internal Pull Up
 * resistor will be enabled.
 * The logic will then work as follows (pseudo code).
 * IF (P10 == 1) {
 *     // This HW has a standard SD Card socket
 *     // Card Detection (CD) signal has its internal pull up enabled
 *     // and works as follows:
 *     IF (CD == 1) THEN {
 *         "There is not card in the socket"
 *     } ELSE {
 *         "a card is present."
 *         // Write Protect (WP) signal works and
 *         // its internal pull up resistor is enabled.
 *         IF (WP == 1) THEN {
 *             "card is protected"
 *         } ELSE {
 *             "card is not protected"
 *         }
 *     }
 * } ELSE {
 *    // This HW has a Micro SD Card socket
 *    // Card Detection (CD) signal has its internal pull up enabled
 *    // and works as follows
 *    IF (CD == 1) THEN "a card is present"
 *    ELSE "There is not card in the socket"
 *    // Write Protect (WP) signal does not work.
 * }
 *
 * Card Detect (CD)
 * Regarding the Card Detect signal, the part being suggested has pins 9 and
 * 10 for that, they are simply a contact closure that gets closed when the
 * card is inserted so one pin (say pin #9) should be tied to ground and Pin
 * #10 should be routed to our SD_CD signal (the pin that is connected to this
 * signal on the microcontroller side should have an internal pull-up resistor
 *
 * P79 (pin 66) uses I/O circuit type E (see circuit image attached).
 * Type E has a pull-up of approximately 50kO. Its state type is H, and
 * power-on reset is Hi-Z.
 *
 * For the microSD socket, both pins are actually connected
 * together internally (so they belong to the same signal), what happens is
 * that when there is no card inserted, that signal is tied to the chassis
 * ground, when you insert a card then that signal is left floating.
 */
#define SD_MISO_PORT    GPIO1PIN_P75
#define SD_MOSI_PORT    GPIO1PIN_P76
#define SD_CLK_PORT     GPIO1PIN_P77
#define SD_CS_PORT      GPIO1PIN_P78
#define SD_CD_PORT      GPIO1PIN_P79
#define SD_WP_PORT      GPIO1PIN_P7A
#define SD_MICROSD_PORT GPIO1PIN_P10

#define SD_MISO_SERIAL  SIN3_0
#define SD_MOSI_SERIAL  SOT3_0
#define SD_CLK_SERIAL   SCK3_0
#define MFS MFS3
/* buffer used by MFS driver for transmitting */
static char MFS_Transmit_Buffer_Data[512];
/* buffer used by MFS driver for storing received bytes */
static char MFS_Receive_Buffer_Data[512];

#define MSD_CS_LOW() Gpio1pin_Put(SD_CS_PORT, 1)
/* Deselect MSD Card: ChipSelect pin high */
#define MSD_CS_HIGH() Gpio1pin_Put(SD_CS_PORT, 0)

/**
 * Initializes the SPI and GPIOs used to drive the microSD card.
 */
static void SPI_Config(void)
{
    en_result_t status;
    stc_mfs_config_t stcConfig;

    /* configure the peripheral to use GPIO */
    Gpio1pin_InitOut(SD_CS_PORT, Gpio1pin_InitVal(1));
    Gpio1pin_InitIn(SD_CD_PORT, Gpio1pin_InitPullup(1));
    Gpio1pin_InitIn(SD_WP_PORT, Gpio1pin_InitPullup(1));
    Gpio1pin_InitIn(SD_MICROSD_PORT, Gpio1pin_InitPullup(1));
    /* Configure SPI3 pins: SCK, MISO and MOSI */
    SetPinFunc_SIN3_0();
    SetPinFunc_SOT3_0();
    SetPinFunc_SCK3_0();
    /* Initialize the SPI mode */
    stcConfig.enMode = MfsSynchronousMaster2;
    stcConfig.enClockSource = MfsUseExternalSerialClockSourceWithBaudRateGenerator;
    stcConfig.u32DataRate = 1000000; /* 1Mbps */
    stcConfig.enParity = MfsParityNone;
    stcConfig.enStartStopBit = MfsNoStartNoStop;
    stcConfig.u8CharLength = MfsEightBits;
    stcConfig.bBitDirection = TRUE; /* TRUE: MSB first */
    stcConfig.bSyncClockInversion = FALSE; /* FALSE: Sampling on rising edge. */
    stcConfig.u8SyncClockWaitTime = MfsSyncWaitOne;
    stcConfig.bSignalSystem = FALSE; /* FALSE: NRZ */
    stcConfig.bSyncClockOutputDelay = TRUE; /* This bit is for SPI */
    stcConfig.pcTxBuffer = MFS_Transmit_Buffer_Data;
    stcConfig.u16TxBufferSize = sizeof(MFS_Transmit_Buffer_Data);
    stcConfig.pcRxBuffer = MFS_Receive_Buffer_Data;
    stcConfig.u16RxBufferSize = sizeof(MFS_Receive_Buffer_Data);
    stcConfig.u16RxCallbackBufferFillLevel = 1;
    stcConfig.enTxCallbackMode = MfsOnTxFinished;
    stcConfig.pfnTxCallback = NULL;
    stcConfig.pfnRxCallback = NULL;
    stcConfig.pfnErrCallback = NULL;
    stcConfig.bSubstLFwithCRLF = FALSE;
    status = Mfs_Init((stc_mfsn_t*)&MFS, &stcConfig);
    while (status != ErrorOk) {};
}

/**
 * Reads 512 bytes of data from the SD card
 *
 * @param  file - hardware interface
 * @param  address - SD card address offset
 * @param  buf - place for data to be stored
 *
 * @return something
 */
int8_t if_readBuf(Interface* file, uint32_t address, uint8_t* buf)
{
  return(sd_readSector(&file->spi, address, buf));
}

/**
 * Writes 512 bytes of data to the SD card
 *
 * @param  file - hardware interface
 * @param  address - SD card address offset
 * @param  buf - data to be written
 *
 * @return something
 */
int8_t if_writeBuf(Interface* file, uint32_t address, uint8_t* buf)
{
  return(sd_writeSector(&file->spi, address, buf));
}

/**
 * Set the position in the file (dummy function)
 *
 * @param  file - hardware interface
 * @param  address - SD card address offset
 *
 * @return something
 */
int8_t if_setPos(Interface* file, uint32_t address)
{
  return(0);
}

/**
 * Utility-function for sending a byte which does not toogle CS.
 * Only needed during card-init. During init,
 * the automatic chip-select is disabled for SSP
 *
 * @param  iface - hardware interface (ignored)
 * @param  outgoing - SD card address offset
 *
 * @return byte received during sending
 */
static uint8_t my_if_spiSend(Interface *iface, uint8_t outgoing)
{
    uint8_t incoming = 0xff;

    Mfs_SynchronousTransfer((stc_mfsn_t*)&MFS, &outgoing, &incoming, 1);

    return incoming;
}

/**
 * Initialize the SPI interface
 *
 * @param  iface - hardware interface (ignored)
 */
uint8_t if_spiInit(Interface *iface)
{
    uint8_t i;

    SPI_Config();
    MSD_CS_HIGH();
    /* Send 20 spi commands with card not selected */
    for (i = 0;i < 21;i++) {
        my_if_spiSend(iface, 0xff);
    }

    return 0;
}

/**
 * Utility-function for sending a byte
 *
 * @param  iface - hardware interface (ignored)
 * @param  outgoing - SD card address offset
 *
 * @return byte received during sending
 */
uint8_t if_spiSend(Interface *iface, uint8_t outgoing)
{
    uint8_t incoming = 0xff;

    MSD_CS_LOW();
    Mfs_SynchronousTransfer((stc_mfsn_t*)&MFS, &outgoing, &incoming, 1);
    MSD_CS_HIGH();

    return incoming;
}

/**
 * inteface initialization
 *
 * @param  iface - hardware interface (ignored)
 * @param  opts - initialization options (ignored)
 *
 * @return 0=configured, -1=failed to init, -2=card not ready
 */
int8_t if_initInterface(Interface* file, uint8_t* opts)
{
    uint32_t sc;

    file->spi.spiHwInterface = (stc_mfsn_t*)&MFS;
	file->spi.spiHwInit = if_spiInit;
	file->spi.spiSendByte = if_spiSend;

    /* init at low speed */
    if_spiInit(file);

    if (sd_Init(file) < 0) {
        /* Card failed to init */
        return(-1);
    }
    if (sd_State(file) < 0) {
        /* Card didn't return the ready state */
        return(-2);
    }
    sd_getDriveSize(file, &sc);
    file->sectorCount = sc / 512;
    if ( (sc % 512) != 0) {
        file->sectorCount--;
    }

    return(0);
}
