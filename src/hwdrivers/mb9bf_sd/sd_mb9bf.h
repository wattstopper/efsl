/**
 * @file
 * @author  Steve Karg
 * @date    March 25, 2014
 * @brief   Header for sd_mb9bf.c
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SD_MB9BF_H
#define __SD_MB9BF_H

/* Includes ------------------------------------------------------------------*/
#include "debug.h"
#include "efs-cfg.h"
#include "sd.h"

/* Exported types ------------------------------------------------------------*/
/*************************************************************\
              hwInterface
               ----------
* FILE* 	imagefile		File emulation of hw interface.
* long		sectorCount		Number of sectors on the file.
\*************************************************************/
struct  hwInterface{
	/*FILE 	*imageFile;*/
	long  	sectorCount;
    SdSpiProtocol spi;
};
typedef struct hwInterface Interface;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
int8_t if_initInterface(Interface* file,uint8_t* opts);
int8_t if_readBuf(Interface* file,uint32_t address,uint8_t* buf);
int8_t if_writeBuf(Interface* file,uint32_t address,uint8_t* buf);
int8_t if_setPos(Interface* file,uint32_t address);
void if_spiInit(Interface *iface);
uint8_t if_spiSend(Interface *iface, uint8_t outgoing);


#endif
